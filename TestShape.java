package Shape;

public class TestShape {
    public static void main(String[] args) {
        Circle c1 = new Circle(5.2);
        System.out.println(c1);
        Circle c2 = new Circle(4.5);
        System.out.println(c2);
        Circle c3 = new Circle(1.5);
        System.out.println(c3);
        Square s1 = new Square(2.0);
        System.out.println(s1);
        Square s2 = new Square(4.0);
        System.out.println(s2);
        Rectangle r1 = new Rectangle(4, 3);
        System.out.println(r1);
        Rectangle r2 = new Rectangle(3, 2);
        System.out.println(r2);

        Shape[] shapes = {c1, c2, c3, s1, s2, r1, r2};
        for(int i = 0; i < shapes.length; i++){
            System.out.println(shapes[i].getName() + " area : " + shapes[i].calArea());
        }
    }
}
