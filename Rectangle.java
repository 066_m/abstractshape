package Shape;

public class Rectangle extends Shape {
    private double w;
    private double h;

    public Rectangle(double w, double h){
        super("Rectangle");
        this.w = w;
        this.h = h;
    }

    public double getW() {
        return w;
    }

    public void setW(double w) {
        this.w = w;
    }

    public double getH() {
        return h;
    }

    public void setH(double h) {
        this.h = h;
    }

    @Override
    public double calArea(){
        return w*h;
    }

    @Override
    public String toString(){
        return "Rectangle{" + "wide=" + w + ",height=" + h + "}";
    }
}
